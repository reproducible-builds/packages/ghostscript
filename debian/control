Source: ghostscript
Section: text
Priority: optional
Maintainer: Debian Printing Team <debian-printing@lists.debian.org>
Build-Depends: cdbs,
 autoconf,
 debhelper,
 dh-buildinfo,
 devscripts,
 libregexp-assemble-perl,
 libimage-exiftool-perl,
 libfont-ttf-perl,
 liblcms2-dev (>= 2.5),
 d-shlibs (>= 0.58~),
 libjpeg-dev,
 libpng-dev,
 libtrio-dev,
 libtiff-dev,
 libz-dev,
 libjbig2dec-dev,
 libijs-dev,
 libexpat-dev,
 libxt-dev,
 libxext-dev,
 libx11-dev,
 libice-dev,
 libsm-dev,
 libopenjpeg-dev,
 libfreetype6-dev,
 libfontconfig1-dev,
 libcups2-dev,
 libcupsimage2-dev,
 libpaper-dev,
 libidn11-dev,
 freeglut3-dev,
 pkg-config
Standards-Version: 3.9.6
Uploaders: Jonas Smedegaard <dr@jones.dk>,
 Michael Gilbert <mgilbert@debian.org>,
 Bastien ROUCARIÈS <roucaries.bastien+debian@gmail.com>
Homepage: http://www.ghostscript.com/
Vcs-Git: git://anonscm.debian.org/git/printing/ghostscript.git
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=printing/ghostscript.git

Package: ghostscript
Architecture: any
Multi-Arch: foreign
Provides: ${cdbs:Provides}
Recommends: ${cdbs:Recommends}
Suggests: ${cdbs:Suggests}
Depends: ${shlibs:Depends},
 ${cdbs:Depends},
 libgs9 (= ${binary:Version}),
 ${misc:Depends}
Description: interpreter for the PostScript language and for PDF
 GPL Ghostscript is used for PostScript/PDF preview and printing.
 Usually as a back-end to a program such as ghostview, it can display
 PostScript and PDF documents in an X11 environment.
 .
 Furthermore, it can render PostScript and PDF files as graphics to be
 printed on non-PostScript printers. Supported printers include common
 dot-matrix, inkjet and laser models.

Package: ghostscript-x
Architecture: any
Depends: ${shlibs:Depends},
 ${cdbs:Depends},
 ${misc:Depends}
Description: interpreter for the PostScript language and for PDF - X11 support
 GPL Ghostscript is used for PostScript/PDF preview and printing.
 Usually as a back-end to a program such as ghostview, it can display
 PostScript and PDF documents in an X11 environment.
 .
 This package contains the GPL Ghostscript output device for X11.

Package: ghostscript-doc
Section: doc
Architecture: all
Depends: ${shlibs:Depends},
 ${cdbs:Depends},
 ${misc:Depends}
Suggests: ${cdbs:Suggests}
Description: interpreter for the PostScript language and for PDF - Documentation
 GPL Ghostscript is used for PostScript/PDF preview and printing.
 Usually as a back-end to a program such as ghostview, it can display
 PostScript and PDF documents in an X11 environment.
 .
 This package contains documentation for GPL Ghostscript, mainly
 targeted developers and advanced users.

Package: libgs9
Section: libs
Architecture: any
Depends: ${shlibs:Depends},
 ${cdbs:Depends},
 ${misc:Depends},
 libgs9-common (= ${source:Version})
Description: interpreter for the PostScript language and for PDF - Library
 GPL Ghostscript is used for PostScript/PDF preview and printing.
 Usually as a back-end to a program such as ghostview, it can display
 PostScript and PDF documents in an X11 environment.
 .
 This package provides the Ghostscript library which makes the
 facilities of GPL Ghostscript available to applications.

Package: libgs9-common
Section: libs
Architecture: all
Depends: ${misc:Depends}
Recommends: ${cdbs:Recommends}
Description: interpreter for the PostScript language and for PDF - common files
 GPL Ghostscript is used for PostScript/PDF preview and printing.
 Usually as a back-end to a program such as ghostview, it can display
 PostScript and PDF documents in an X11 environment.
 .
 This package provides common architecture-independent files needed by
 the GPL Ghostscript library.
 .
 By default, GPL Ghostscript uses a font from the fonts-droid package to
 approximate glyphs in PDFs for which the requested CJK TrueType font
 is missing.  If the fonts-droid package is not installed, these glyphs
 will be rendered as bullets.

Package: libgs-dev
Section: libdevel
Architecture: any
Depends: ${devlibs:Depends},
 ${cdbs:Depends},
 ${misc:Depends}
Description: interpreter for the PostScript language and for PDF - Development Files
 GPL Ghostscript is used for PostScript/PDF preview and printing.
 Usually as a back-end to a program such as ghostview, it can display
 PostScript and PDF documents in an X11 environment.
 .
 This package provides the development files for the GPL Ghostscript
 library which makes the facilities of GPL Ghostscript available to
 applications.

Package: ghostscript-dbg
Architecture: any
Section: debug
Priority: extra
Depends: ${shlibs:Depends},
 ${cdbs:Depends},
 libgs9 (= ${binary:Version}) |
 ghostscript (= ${binary:Version}) |
 ghostscript-x (= ${binary:Version}),
 ${misc:Depends}
Description: interpreter for the PostScript language and for PDF - Debug symbols
 GPL Ghostscript is used for PostScript/PDF preview and printing.
 Usually as a back-end to a program such as ghostview, it can display
 PostScript and PDF documents in an X11 environment.
 .
 This package contains the debugging symbols for ghostscript,
 ghostscript-x, and libgs9.
